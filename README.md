# SatNOGS `rpi_imager.json` hosting page

This repository is using GitLab Pages to host a `rpi_imager.json` with SatNOGS Images for the [rpi-imager](https://github.com/raspberrypi/rpi-imager).
It is fetching the built images by [satnogs/satnogs-pi-gen](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen), extract the filesize & checksums
and writes them to `public/rpi_imager.json`.

## Usage

The hosted `rpi_imager.json` file can be found at <https://kerel-fs.gitlab.io/rpi-imager-satnogs-pages-test/rpi-imager.json>.

Usage:
```
rpi-imager --repo https://kerel-fs.gitlab.io/rpi-imager-satnogs-pages-test/rpi-imager.json
```

# License

MIT
