#!/usr/bin/env python3
import os
import json
import zipfile
import subprocess

from pathlib import Path


RPI_IMAGER_JSON_PATH = "./public/rpi-imager.json"

SATNOGS_IMAGE_NAME = "SatNOGS (stable)"
SATNOGS_IMAGE_DESCRIPTION = "Raspbian-based SatNOGS Image 2022091000"
SATNOGS_IMAGE_URL="https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/-/jobs/artifacts/2022091000/raw/deploy/image_2022-09-10-Raspbian-SatNOGS-lite.zip?job=release&workaround=.zip"
SATNOGS_IMAGE_RELEASE_DATE="2022-09-10"

SHA256SUMS_PATH = "./deploy/sha256sums"


ADDITIONAL_IMAGES = [
  {
    "name": "SatNOGS (for Rasperry Pi 4)",
    "description": "Rasbpian-based SatNOGS Image 2022022700 + Pi4 workaround (updated firmware)",
    "url": "https://cloud.libre.space/s/4cfcK6H4joCdyCm/download/2022-04-05-PE0SAT_Raspbian-SatNOGS-lite.zip",
    "icon": "https://gitlab.com/uploads/-/system/group/avatar/1846145/7274400.png?width=40",
    "release_date": SATNOGS_IMAGE_RELEASE_DATE,
    "extract_size": 3758096384,
    "extract_sha256": "4638ec96df3195d2654a457523bd4433970e0420c1ac40e9793e07be8e94a62c",
    "image_download_size": 1048901025,
    "image_download_sha256": "c769ead89686f6b232a79f23637dedec018aa73982ec31e387e61d4d60029d52"
  }
]


if __name__ == "__main__":
    image_download_path = './deploy/image_2020-12-27-Raspbian-SatNOGS-lite.zip'
    image_extract_path = './tmp/2020-12-27-Raspbian-SatNOGS-lite.img'

    # Read checksum from file
    checksums = Path(SHA256SUMS_PATH).read_text()
    image_download_sha256 = checksums.split('\n')[1].split()[0]
    # Get filesize from filesystem
    image_download_size = os.path.getsize(image_download_path)

    # Extract image
    with zipfile.ZipFile('./deploy/image_2020-12-27-Raspbian-SatNOGS-lite.zip', 'r') as ff:
        ff.extractall('./tmp')

    # Get filesize from filesystem
    extract_size = os.path.getsize(image_extract_path)

    # Calculate checksum
    checksum_process = subprocess.run(["sha256sum", "tmp/2020-12-27-Raspbian-SatNOGS-lite.img"], capture_output=True)
    extract_sha256 = checksum_process.stdout.decode('ascii').split()[0]

    data = {
      "os_list": [
        {
          "name": SATNOGS_IMAGE_NAME,
          "description": SATNOGS_IMAGE_DESCRIPTION,
          "url": SATNOGS_IMAGE_URL,
          "icon": "https://gitlab.com/uploads/-/system/group/avatar/1846145/7274400.png?width=40",
          "release_date": SATNOGS_IMAGE_RELEASE_DATE,
          "extract_size": extract_size,
          "extract_sha256": extract_sha256,
          "image_download_size": image_download_size,
          "image_download_sha256": image_download_sha256
        }
      ]
    }

    data['os_list'].extend(ADDITIONAL_IMAGES)

    with open(RPI_IMAGER_JSON_PATH, 'w') as output_file:
        json.dump(data, output_file, indent=2)
